#
#
# Build:
#  docker build --force-rm -t "cyrille37/etherpadlite-fullstack:v1.0" -f etherpadlite-fullstack.dockerfile .
# Run:
#  docker run --rm -ti -p "9001:9001" -v "${docker.data}/ep_mysql:/var/lib/mysql" -v "${docker.data}/ep_mysqld:/var/run/mysqld" -v "${docker.data}/ep_var:/opt/etherpad-lite/var"  cyrille37/etherpadlite-fullstack:v1.0
#
#  dockerdata="/some/path/docker.data" && docker create -p "9001:9001" -v ${dockerdata}/ep_mysql:/var/lib/mysql -v ${dockerdata}/ep_mysqld:/var/run/mysqld -v ${dockerdata}/ep_mysql_confd:/etc/mysql/conf.d -v ${dockerdata}/ep_var:/opt/etherpad-lite/var cyrille37/etherpadlite-fullstack:v1.0
#

FROM cyrille37/etherpadlite-fullstack-base:v1.0

MAINTAINER Cyrille Giquello <cyrille@giquello.fr>

#
# Launcher script
#

COPY docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["/usr/bin/supervisord","-c","/etc/supervisor/conf.d/supervisord.conf","-n"]
