# etherpadlite-fullstack Docker image

Une image pour faire tourner [etherpad-lite](https://github.com/ether/etherpad-lite), avec:

* Etherpad-lite 1.5.7
* abiword et tidy pour l'export des pads
* MariaDb 10.1, optimisée pour MyIsam
* Nodejs 0.12
* supervisord, pour gérer les 2 processus (mariadb et etherpad-lite)

## Configuration

Tous les paramètres des éléments du conteneur sont définis en dur.

Pour lancer un contener il suffit de coréler 2 volumes et le port http, et hop ça roule ;-)

### VOLUME /var/lib/mysql

Nécessaire.
Pour les données de Mysql.

### VOLUME /opt/etherpad-lite/var

Nécessaire.
Pour les données de etherpad-lite.
Et son fichier de configuration `settings.json`.

### VOLUME /var/run/mysqld

Optionnel.
Pour avoir accès à la socket file de MariaDb/Mysql et ainsi se connecter à la base de données depuis le Host.
Pour faire des sauvegardes par exemple ;-)

### VOLUME /etc/mysql/conf.d

Optionnel.
Pour régler la configuration de MariaDb/Mysql

### VOLUME /etc/supervisor/conf.d

Optionnel.
Pour régler la configuration de supervisor

### Pads Admin

Pour activer l'interface d'adminitration des pads
il faut ajouter dans `settings.json` :

```
  "users": {
    "le nom du user": {
      "password": "le mot de passe",
      "is_admin": true
    }
  },
```

Attention, le conteneur doit être arrêté pour modifier le fichier `settings.json`
car etherpad-lite écrase ce fichier lorsqu'il s'arrête.

Ou passer par l'interface d'administration qui permet d'éditer directement le fichier `settings.json`.

## Build

D'abord construire l'image de base avec tous les logiciels:

```
docker build --force-rm -t "cyrille37/etherpadlite-fullstack-base:v1.0" -f etherpadlite-fullstack-base.dockerfile .
```

Ensuite construire l'image qui permet de bidouiller les configurations sans re-installer les logiciels:

```
docker build --force-rm -t "cyrille37/etherpadlite-fullstack:v1" -f etherpadlite-fullstack.dockerfile .
```

## Run

Remplacer `<path to data folder>` par le chemin du dossier prévu pour stocker les données de mysql et etherpad-lite.

**ATTENTION** aux droits d'accès à ce dossier, le conteneur doit pouvoir y écrire.

```
docker run --rm -ti -p "9001:9001" -v "<path to data folder>/ep_mysql:/var/lib/mysql" -v "<path to data folder>/ep_var:/opt/etherpad-lite/var" cyrille37/etherpadlite-fullstack:v1
```

Pour permettre l'accès à la base de données depuis l'extérieur du conteneur, il suffit d'exposer le dossier `/var/run/mysqld`:

```
docker run --rm -ti -p "9001:9001" \
	-v "<path to data folder>/ep_mysql_var:/var/run/mysqld \
	-v "<path to data folder>/ep_mysql:/var/lib/mysql" \
	-v "<path to data folder>/ep_var:/opt/etherpad-lite/var" \
	cyrille37/etherpadlite-fullstack:v1
```

## Autre

### Avec nginx-proxy

Lancer le proxy:

```
$ docker run -d -p 80:80 -v /var/run/docker.sock:/tmp/docker.sock:ro jwilder/nginx-proxy
```

Lancer etherpadlite-fullstack, en remplaçant `<un nom de domaine>` par un nom de domaine valide.

```
$ docker run --rm -ti \
	-e VIRTUAL_HOST=<un nom de domaine> -e VIRTUAL_PORT=9001 \
	-v "<path to data folder>/ep_mysql_var:/var/run/mysqld \
	-v "<path to data folder>/ep_mysql:/var/lib/mysql" \
	-v "<path to data folder>/ep_var:/opt/etherpad-lite/var" \
	cyrille37/etherpadlite-fullstack:v1
```

Sur votre machine cliente (pas le Host Docker mais celle avec une souris et Firefox) ajouter dans /etc/hosts `<ip du host docker> <un nom de domaine>`.

Et hop, dans un navigateur http://un_nom_de_domaine/ on peut créer un Pad ;-)

Pour de la prod ça pourrait donner une ligne comme ça:

```
dockerdata="/home/cyrille/docker.data" && docker create \
 --restart=unless-stopped \
 -m 1G \
 -e VIRTUAL_HOST=pad.bidon -e VIRTUAL_PORT=9001 \
 -p "9001:9001" \
 -v ${dockerdata}/ep_mysql:/var/lib/mysql \
 -v ${dockerdata}/ep_mysqld:/var/run/mysqld \
 -v ${dockerdata}/ep_mysql_confd:/etc/mysql/conf.d \
 -v ${dockerdata}/ep_var:/opt/etherpad-lite/var \
 cyrille37/etherpadlite-fullstack:v1.0
```
