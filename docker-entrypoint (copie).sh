#!/bin/bash
set -eo pipefail

: ${MYSQL_ROOT_PASSWORD:=root}

: ${ETHERPAD_TITLE:="Yep! Another Pad"}
: ${ETHERPAD_DB_USER:=etherpadlite}
: ${ETHERPAD_DB_PASSWORD:=etherpadlite}
: ${ETHERPAD_DB_NAME:=etherpadlite}
: ${MYSQL_SOCK:=/var/run/mysqld/mysqld.sock}

if [ "$1" = 'bash' ]; then
	exec "$@"
	exit 0
fi

#
# MySql configuration
#

echo "Checking if Mysql data are present..."

DATADIR="$(mysqld --verbose --help --log-bin-index=`mktemp -u` 2>/dev/null | awk '$1 == "datadir" { print $2; exit }')"

if [ ! -d "$DATADIR/mysql" ]; then

	echo 'Initializing Mysql data...'

	mkdir -p "$DATADIR"
	chown -R mysql:mysql "$DATADIR"

	echo "\t Initializing database..."

	mysql_install_db --user=mysql --datadir="$DATADIR" --rpm

	echo "\t Lauching a temporary Mysql instance..."

	mysqld --skip-networking &
	pid="$!"

	mysql=( mysql --protocol=socket -uroot )

	# Wait for mysql to start
	for i in {30..0}; do
		if echo 'SELECT 1' | "${mysql[@]}" &> /dev/null; then
			break
		fi
		echo 'MySQL init process in progress...'
		sleep 1
	done
	if [ "$i" = 0 ]; then
		echo >&2 'MySQL init process failed.'
		exit 1
	fi

	if [ -z "$MYSQL_INITDB_SKIP_TZINFO" ]; then
		# sed is for https://bugs.mysql.com/bug.php?id=20545
		mysql_tzinfo_to_sql /usr/share/zoneinfo | sed 's/Local time zone must be set--see zic manual page/FCTY/' | "${mysql[@]}" mysql
	fi

	"${mysql[@]}" <<-EOSQL
		-- What's done in this file shouldn't be replicated
		--  or products like mysql-fabric won't work
		SET @@SESSION.SQL_LOG_BIN=0;
		DELETE FROM mysql.user ;
		CREATE USER 'root'@'%' IDENTIFIED BY '${MYSQL_ROOT_PASSWORD}' ;
		GRANT ALL ON *.* TO 'root'@'%' WITH GRANT OPTION ;
		DROP DATABASE IF EXISTS test ;
		FLUSH PRIVILEGES ;
	EOSQL

	echo "\t Initializing Etherpad-lite data..."

	if [ ! -z "$MYSQL_ROOT_PASSWORD" ]; then
		mysql+=( -p"${MYSQL_ROOT_PASSWORD}" )
	fi

	echo "CREATE DATABASE IF NOT EXISTS \`$ETHERPAD_DB_NAME\` ;" | "${mysql[@]}"
	mysql+=( "$ETHERPAD_DB_NAME" )

	echo "CREATE USER '$ETHERPAD_DB_USER'@'%' IDENTIFIED BY '$ETHERPAD_DB_PASSWORD' ;" | "${mysql[@]}"
	echo "GRANT ALL ON \`$ETHERPAD_DB_NAME\`.* TO '$ETHERPAD_DB_USER'@'%' ;" | "${mysql[@]}"
	echo 'FLUSH PRIVILEGES ;' | "${mysql[@]}"

	echo "\t Stopping the temporary Mysql instance..."

	if ! kill -s TERM "$pid" || ! wait "$pid"; then
		echo >&2 'MySQL init process failed.'
		exit 1
	fi

	echo
	echo 'MySQL and Etherpad-lite init process done. Ready for start up.'
	echo

fi

chown -R mysql:mysql "$DATADIR"

#
# Etherpad-lite configuration
#
# supervisor example: https://github.com/ether/etherpad-docker/
#

echo "Check if etherpad-lite settings.json exists ..."

if [ ! -f /opt/etherpad-lite/var/settings.json ]; then

	echo "Creating etherpad-lite settings.json ..."

	# Etherpad-lite settings template:
	# https://github.com/ether/etherpad-lite/blob/1.5.7/settings.json.template

	# Etherpad-lite with Mysql via socket
	# https://github.com/ether/etherpad-lite/wiki/How-to-use-Etherpad-Lite-with-MySQL

	# ep 1.5.7 sessionKey deprecated
	#: ${ETHERPAD_SESSION_KEY:=$(node -p "require('crypto').randomBytes(32).toString('hex')")}

	cat <<- EOF > /opt/etherpad-lite/var/settings.json
	{
	  "title": "${ETHERPAD_TITLE}",
	  /* Default Pad behavior, users can override by changing */
	  "padOptions": {
	    "noColors": false,
	    "showControls": true,
	    "showChat": true,
	    "showLineNumbers": true,
	    "useMonospaceFont": false,
	    "userName": false,
	    "userColor": false,
	    "rtl": false,
	    "alwaysShowChat": false,
	    "chatAndUsers": false,
	    "lang": "en-gb"
	  },
	  /* The toolbar buttons configuration.
	  "toolbar": {
	    "left": [
	      ["bold", "italic", "underline", "strikethrough"],
	      ["orderedlist", "unorderedlist", "indent", "outdent"],
	      ["undo", "redo"],
	      ["clearauthorship"]
	    ],
	    "right": [
	      ["importexport", "timeslider", "savedrevision"],
	      ["settings", "embed"],
	      ["showusers"]
	    ],
	    "timeslider": [
	      ["timeslider_export", "timeslider_returnToPad"]
	    ]
	  },
	  */

	  "ip": "0.0.0.0",
	  "port" : 9001,
	  "dbType" : "mysql",
	  "dbSettings" : {
	    "user"    : "${ETHERPAD_DB_USER}",
	    "port" : "${MYSQL_SOCK}",
	    "password": "${ETHERPAD_DB_PASSWORD}",
	    "database": "${ETHERPAD_DB_NAME}"
	  },

	  "users": {
	    /* Uncomment and change password (and username) to enable admin panel
	    "admin": {
	    "password": "secret",
	      "is_admin": true
	    }
	    */
	  },

	  /* The log level we are using, can be: DEBUG, INFO, WARN, ERROR */
	  "loglevel": "WARN",

	  /* This is the path to the Abiword executable. Setting it to null, disables abiword.
	     Abiword is needed to advanced import/export features of pads*/
	  "abiword" : "/usr/bin/abiword",

	  /* This is the path to the Tidy executable. Setting it to null, disables Tidy.
	     Tidy is used to improve the quality of exported pads*/
	  "tidyHtml" : "/usr/bin/tidy",

	  /*when you use NginX or another proxy/ load-balancer set this to true*/
	  "trustProxy" : true,

	  /* Privacy: disable IP logging */
	  "disableIPlogging" : true,

	  // restrict socket.io transport methods
	  "socketTransportProtocols" : ["xhr-polling", "jsonp-polling", "htmlfile"],

	  // Allow Load Testing tools to hit the Etherpad Instance.  Warning this will disable security on the instance.
	  "loadTest": false

	}
	EOF


fi

if [ -f /opt/etherpad-lite/settings.json ]; then
	rm /opt/etherpad-lite/settings.json
fi
ln -s /opt/etherpad-lite/var/settings.json /opt/etherpad-lite/settings.json

echo "All initialisation checked and done."

# exec /usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf -n
exec "$@"
