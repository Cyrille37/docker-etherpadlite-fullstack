#
# A Docker image to make a base for an Etherpad-lite full image
#
# MariaDB
# Nodejs
# Etherpad-lite
#
# Build:
#  docker build --force-rm -t "cyrille37/etherpadlite-fullstack-base:v1.0" -f etherpadlite-fullstack-base.dockerfile .
#
# 2016-05-07 cyrille@giquello.fr
#

FROM debian:jessie

MAINTAINER Cyrille Giquello <cyrille@giquello.fr>

ENV NODE_VERSION 0.12
ENV ETHERPAD_VERSION 1.5.7
ENV MARIADB_MAJOR 10.1

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update \
	&& apt-get upgrade -y \
	&& apt-get install -y curl unzip nano

#
# Install supervisor
#

RUN apt-get install -y supervisor

#
# Install MariaDb
# Inspired from https://hub.docker.com/_/mariadb/
#

# add our user and group first to make sure their IDs get assigned consistently, regardless of whatever dependencies get added
RUN groupadd -r mysql && useradd -r -g mysql mysql

# the "/var/lib/mysql" stuff here is because the mysql-server postinst doesn't have
# an explicit way to disable the mysql_install_db codepath
# besides having a database already "configured" (ie, stuff in /var/lib/mysql/mysql)
# also, we set debconf keys to make APT a little quieter

RUN	{ \
	echo mariadb-server-$MARIADB_MAJOR mysql-server/root_password password 'root'; \
	echo mariadb-server-$MARIADB_MAJOR mysql-server/root_password_again password 'root'; \
	} | debconf-set-selections \
	&& apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 0xcbcb082a1bb943db \
	&& echo "deb [arch=amd64,i386] http://ftp.igh.cnrs.fr/pub/mariadb/repo/$MARIADB_MAJOR/debian jessie main" > /etc/apt/sources.list.d/mariadb.list \
	&& apt-get update \
	&& apt-get install -y mariadb-server-$MARIADB_MAJOR \
	&& rm -rf /var/lib/mysql \
	&& mkdir /var/lib/mysql

VOLUME /var/lib/mysql

#
# Install nodejs
#

RUN curl -sL https://deb.nodesource.com/setup_${NODE_VERSION} | bash -
RUN apt-get install -y nodejs

#
# Install etherpad-lite
#

WORKDIR /opt/

RUN curl -SL https://github.com/ether/etherpad-lite/archive/${ETHERPAD_VERSION}.zip > etherpad.zip \
	&& unzip etherpad && rm etherpad.zip \
	&& mv etherpad-lite-${ETHERPAD_VERSION} etherpad-lite

WORKDIR etherpad-lite

RUN bin/installDeps.sh && rm settings.json

# Etherpad-lite helper tools for exporting pads

RUN apt-get install -y abiword tidy \
	&& apt-get remove -y abiword-plugin-mathview abiword-plugin-grammar

#
# Clean up
#

RUN apt-get remove --purge -y xdg-user-dirs x11-common sgml-base libv4lconvert0 \
	&& apt-get autoremove --purge -y \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

#
# Configuration templates
#

COPY docker.templates /docker.templates

COPY docker.templates/mariadb/99-etherpad-lite.cnf /etc/mysql/conf.d/99-etherpad-lite.cnf
COPY docker.templates/supervisord/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY docker.templates/etherpad-lite/settings.json /opt/etherpad-lite/settings.json

#
# Expose services's
#  - configuration folders
#  - and data folders
#

# Mysql/MariaDb

VOLUME /var/lib/mysql
VOLUME /var/run/mysqld
VOLUME /etc/mysql/conf.d

# Supervisord

VOLUME /etc/supervisor/conf.d

# Etherpad-lite

VOLUME /opt/etherpad-lite/var
EXPOSE 9001
