#!/bin/bash
set -eo pipefail

if [ "$1" = 'bash' ]; then
	exec "$@"
	exit 0
fi

: ${MYSQL_ROOT_PASSWORD:=root}
: ${MYSQL_SOCK:=/var/run/mysqld/mysqld.sock}

: ${ETHERPAD_TITLE:="Yep! Another Pad"}
: ${ETHERPAD_DB_NAME:=etherpadlite}
: ${ETHERPAD_DB_USER:=etherpadlite}
: ${ETHERPAD_DB_PASSWORD:=etherpadlite}

#
# Supervisord configuration
#

if [ ! -w /etc/supervisor/conf.d ] ; then
	echo 'Volume for /etc/supervisor/conf.d is NOT writable' ;
	exit 1 ;
fi
if [ ! -f /etc/supervisor/conf.d/supervisord.conf ]; then
	cp docker.templates/supervisord/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
fi

#
# MariaDb/Mysql configuration
#

if [ ! -w /etc/mysql/conf.d ] ; then
	echo 'Volume for /etc/mysql/conf.d is NOT writable' ;
	exit 1 ;
fi
if [ ! -f /etc/mysql/conf.d/99-etherpad-lite.cnf ]; then
	cp /docker.templates/mariadb/99-etherpad-lite.cnf /etc/mysql/conf.d/99-etherpad-lite.cnf
fi

#
# Etherpad-lite configuration
#
# supervisor example: https://github.com/ether/etherpad-docker/
#

echo "Check if etherpad-lite settings.json exists ..."

if [ ! -w /opt/etherpad-lite/var ] ; then
	echo 'Volume for /opt/etherpad-lite/var is NOT writable' ;
	exit 1 ;
fi

if [ ! -f /opt/etherpad-lite/var/settings.json ]; then

	echo "Creating etherpad-lite settings.json ..."

	cat /docker.templates/etherpad-lite/settings.json \
		| sed "s#%%ETHERPAD_TITLE%%#${ETHERPAD_TITLE}#g" \
		| sed "s/%%ETHERPAD_DB_USER%%/${ETHERPAD_DB_USER}/g" \
		| sed "s/%%ETHERPAD_DB_PASSWORD%%/${ETHERPAD_DB_PASSWORD}/g" \
		| sed "s/%%ETHERPAD_DB_NAME%%/${ETHERPAD_DB_NAME}/g" \
		| sed "s#%%MYSQL_SOCK%%#${MYSQL_SOCK}#g" \
		>/opt/etherpad-lite/var/settings.json

fi

if [ -f /opt/etherpad-lite/settings.json ]; then
	rm /opt/etherpad-lite/settings.json
fi
ln -s /opt/etherpad-lite/var/settings.json /opt/etherpad-lite/settings.json

#
# MySql data initialization
#

if [ ! -w /var/run/mysqld ] ; then
	echo "Volume for /var/run/mysqld is NOT writable" ;
	exit 1 ;
fi

echo "Checking if Mysql data are present..."

DATADIR="$(mysqld --verbose --help --log-bin-index=`mktemp -u` 2>/dev/null | awk '$1 == "datadir" { print $2; exit }')"

if [ ! -w $DATADIR ] ; then
	echo "Volume for $DATADIR is NOT writable" ;
	exit 1 ;
fi

if [ ! -d "$DATADIR/mysql" -o ! -d "$DATADIR/${ETHERPAD_DB_NAME}" ]; then

	echo 'Initializing Mysql data...'

	mkdir -p "$DATADIR"
	chown -R mysql:mysql "$DATADIR"

	echo 'Running mysql_install_db...'

	mysql_install_db --user=mysql --datadir="$DATADIR" --rpm

	echo 'Lauching a temporary Mysql instance...'

	mysqld --skip-networking &
	pid="$!"

	mysql=( mysql --protocol=socket -uroot )

	# Wait for mysql to start
	for i in {30..0}; do
		if echo 'SELECT 1' | "${mysql[@]}" &> /dev/null; then
			break
		fi
		echo 'MySQL init process in progress...'
		sleep 1
	done
	if [ "$i" = 0 ]; then
		echo >&2 'MySQL init process failed.'
		exit 1
	fi

	echo 'Initializing Mysql tzinfo data...'

	if [ -z "$MYSQL_INITDB_SKIP_TZINFO" ]; then
		# sed is for https://bugs.mysql.com/bug.php?id=20545
		mysql_tzinfo_to_sql /usr/share/zoneinfo | sed 's/Local time zone must be set--see zic manual page/FCTY/' | "${mysql[@]}" mysql
	fi

	echo 'Initializing Mysql root user...'

	"${mysql[@]}" <<-EOSQL
		-- What's done in this file shouldn't be replicated
		--  or products like mysql-fabric won't work
		SET @@SESSION.SQL_LOG_BIN=0;
		DELETE FROM mysql.user ;
		CREATE USER 'root'@'%' IDENTIFIED BY '${MYSQL_ROOT_PASSWORD}' ;
		GRANT ALL ON *.* TO 'root'@'%' WITH GRANT OPTION ;
		DROP DATABASE IF EXISTS test ;
		FLUSH PRIVILEGES ;
	EOSQL

	echo 'Initializing Etherpad-lite data...'

	if [ ! -z "$MYSQL_ROOT_PASSWORD" ]; then
		mysql+=( -p"${MYSQL_ROOT_PASSWORD}" )
	fi

	echo "CREATE DATABASE IF NOT EXISTS \`$ETHERPAD_DB_NAME\` ;" | "${mysql[@]}"
	mysql+=( "$ETHERPAD_DB_NAME" )

	echo "CREATE USER '$ETHERPAD_DB_USER'@'%' IDENTIFIED BY '$ETHERPAD_DB_PASSWORD' ;" | "${mysql[@]}"
	echo "GRANT ALL ON \`$ETHERPAD_DB_NAME\`.* TO '$ETHERPAD_DB_USER'@'%' ;" | "${mysql[@]}"
	echo 'FLUSH PRIVILEGES ;' | "${mysql[@]}"

	echo 'Stopping the temporary Mysql instance...'

	if ! kill -s TERM "$pid" || ! wait "$pid"; then
		echo >&2 'MySQL init process failed.'
		exit 1
	fi

	echo
	echo 'MySQL and Etherpad-lite init process done. Ready for start up.'
	echo

fi

chown -R mysql:mysql "$DATADIR"

#
# Done
#

echo "All initialisation checked and done."

# exec /usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf -n
exec "$@"
